package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Group;
import com.vidbox.model.Video;

public class VideoGroupShareScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel groupLabel;
	private JComboBox<String> openGroupComboBox;
	private JButton shareButton;
	private JButton cancelButton;
	private ArrayList<Group> openGroupList;
	private Video video;
	
	public VideoGroupShareScreen(ArrayList<Group> openGroupList, Video video) {
		this.video = video;
		this.openGroupList = openGroupList;
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		groupLabel = new JLabel("Group");
		openGroupComboBox = new JComboBox<String>();
		populateOpenGroupComboBox();
		createCancelButton();
		createShareButton();
		
		mainPanel.add(groupLabel);
		mainPanel.add(openGroupComboBox);
		
		buttonPanel.add(shareButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		setTitle(video.getVideoTitle()+" Group Share");
		setSize(400,400);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	private void populateOpenGroupComboBox() {
		for (int index = 0; index <openGroupList.size(); index++) {
			openGroupComboBox.addItem(openGroupList.get(index).getGroupName());
		}
	}
	
	private void createShareButton() {
		shareButton = new JButton("Share");
		shareButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean result = VidBoxHttpClient.getInstance().shareVideoGroup(openGroupList.get(openGroupComboBox.getSelectedIndex()).getGroupId(), 
						video.getVideoId());
				if (result) {
					JOptionPane.showMessageDialog(VideoGroupShareScreen.this, "Your video was "
							+ "shared successfully in "+openGroupList.get(openGroupComboBox.getSelectedIndex()).getGroupName());
					VideoGroupShareScreen.this.dispose();
				} else {
					JOptionPane.showMessageDialog(VideoGroupShareScreen.this,"Your video couldn't be shared");
				}
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				VideoGroupShareScreen.this.dispose();
			}
			
		});
	}
	

}
