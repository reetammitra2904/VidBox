package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Group;

public class SendGroupInvitationScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel userLabel;
	private JTextField user;
	private JLabel invitationMessageLabel;
	private JTextArea invitationMessage;
	private JButton submitButton;
	private JButton cancelButton;
	private Group group;
	
	public SendGroupInvitationScreen(Group group) {
		this.group = group;
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		userLabel = new JLabel("User:");
		user = new JTextField(20);
		invitationMessageLabel = new JLabel("Message:");
		invitationMessage = new JTextArea(10,20);
		createSubmitButton();
		createCancelButton();
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(userLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(user, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(invitationMessageLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(invitationMessage, mainPanelConstraints);
		
		buttonPanel.add(submitButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		setSize(500,500);
		setTitle(group.getGroupName()+" invitation");
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	private void createSubmitButton() {
		submitButton = new JButton("Submit");
		submitButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean result = VidBoxHttpClient.getInstance().sendInvitation(user.getText(), group.getGroupId(), invitationMessage.getText());
				if (result) {
					JOptionPane.showMessageDialog(SendGroupInvitationScreen.this, "Your invitation has been sent successfully");
					SendGroupInvitationScreen.this.dispose();
				} else {
					JOptionPane.showMessageDialog(SendGroupInvitationScreen.this, "This username doesn't exist");
				}
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SendGroupInvitationScreen.this.dispose();
				
			}
			
		});
	}

}
