package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Group;
import com.vidbox.model.GroupInvitation;
import com.vidbox.model.LoginResponse;
import com.vidbox.model.Video;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

public class HomeScreen extends JFrame{
	
	private static HomeScreen homeScreen = null;
	
	public static String username = "";
	private LoginResponse loginResponse;
	private JPanel mainPanel;
	private JSplitPane splitPane;
	private JPanel informationPanel;
	private JTree informationTree;
	private JPanel buttonPanel;
	private JButton uploadButton;
	private JButton watchVideoButton;
	private JButton createGroupButton;
	private JButton sendInvitationButton;
	private JButton groupShareButton;
	private JButton peerShareButton;
	private JButton joinGroupButton;
	private JTable videoListTable;
	private JScrollPane scrollPane;
	private HashMap<Integer, Video> videoIdIndexMap = new HashMap<Integer, Video>();
	private DefaultTableModel defaultTableModel;
	private DefaultTreeModel defaultTreeModel;
	public static HashMap<Integer, Group> adminGroupIdMap = new HashMap<Integer, Group>();
	private HashMap<Integer, Group> memberGroupIdMap = new HashMap<Integer, Group>();
	private HashMap<Integer, GroupInvitation> groupInvitationIdMap = new HashMap<Integer, GroupInvitation>();
	private int currentInvitationNodeIndex = -1;
	private Group currentSelectedAdminGroup;
	public static ArrayList<Group> openGroupList = new ArrayList<Group>();
	private String getLastSelectedTreeNode = "";
	private int getLastSelectedNodeIndex = -1;
	
	private HomeScreen(String username, LoginResponse loginResponse) {
		this.username = username;
		this.loginResponse = loginResponse;
		init();
	}
	
	public static HomeScreen getInstance(String username, LoginResponse loginResponse) {
		if (homeScreen == null) {
			homeScreen = new HomeScreen(username, loginResponse);
		}
		return homeScreen;
	}
	
	public static HomeScreen getInstance() {
		return homeScreen;
	}
	
	private void init() {
		
		mainPanel = new JPanel(new GridLayout());
		buttonPanel = new JPanel();
		informationPanel = new JPanel(new GridLayout());
		splitPane = new JSplitPane();
		
		createUploadButton();
		createWatchVideoButton();
		createSendInvitationButton();
		createGroupButton();
		createVideoListTable();
		createInformationTree();
		createGroupShareButton();
		createPeerShareButton();
		createJoinGroupButton();
		
		scrollPane = new JScrollPane(videoListTable);
		mainPanel.add(scrollPane);
		
		buttonPanel.add(uploadButton);
		buttonPanel.add(watchVideoButton);
		buttonPanel.add(createGroupButton);
		buttonPanel.add(sendInvitationButton);
		buttonPanel.add(groupShareButton);
		buttonPanel.add(peerShareButton);
		buttonPanel.add(joinGroupButton);
		
		informationPanel.add(informationTree);
		
		splitPane.setLeftComponent(informationPanel);
		splitPane.setRightComponent(mainPanel);
		splitPane.setDividerLocation(0.3);
		
		informationPanel.setBorder(new LineBorder(Color.RED, 1, true));
		mainPanel.setBorder(new LineBorder(Color.RED, 1, true));
		buttonPanel.setBorder(new LineBorder(Color.RED, 1, true));
		
		
		getContentPane().add(splitPane, new BorderLayout().CENTER);
	    getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setVisible(true);
		setLocationRelativeTo(null);
		setTitle("Home");
		setVisible(true);
		restoreDefaults();
	}
	
	private void restoreDefaults() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                splitPane.setDividerLocation(splitPane.getSize().height /2);
                //mainSplittedPane.setDividerLocation(mainSplittedPane.getSize().width /2);
            }
        });
    }
	
	private void createUploadButton() {
		uploadButton = new JButton("Upload");
		uploadButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new VideoUploadScreen();
			}
			
		});
	}
	
	private void createWatchVideoButton() {
		watchVideoButton = new JButton("Watch Video");
		watchVideoButton.setEnabled(false);
		watchVideoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int index = Integer.parseInt(videoListTable.getModel().getValueAt(videoListTable.getSelectedRow(), 0).toString());
				System.out.println("Fetching from videoIdIndexMap is "+videoIdIndexMap.get(index).getVideoId().trim());
				VidBoxHttpClient.getInstance().fetchVideo(videoIdIndexMap.get(index).getVideoId());			
			}
			
		});
	}
	
	private void createVideoListTable() {
		defaultTableModel = new DefaultTableModel();
		defaultTableModel.addColumn("S.No.");
		defaultTableModel.addColumn("Title");
		defaultTableModel.addColumn("Description");
		int counter = 0;
		for (int index = 0; index < loginResponse.getVideoList().getVideoList().size(); index++) {
			counter++;
			Video video = loginResponse.getVideoList().getVideoList().get(index);
			String[] row = {String.valueOf(counter), video.getVideoTitle(), video.getVideoDescription()};
			defaultTableModel.addRow(row);
			videoIdIndexMap.put(counter, video);
		}
		videoListTable = new JTable(defaultTableModel);
		videoListTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			 public void valueChanged(ListSelectionEvent event) {
				 watchVideoButton.setEnabled(true);
				 if (videoListTable.getSelectedRow() != -1) {
					 watchVideoButton.setEnabled(true);
				 } else {
					 watchVideoButton.setEnabled(false);
				 }
			 
			 }
			});
	}
	
	public void refreshListTable(Video video, String nodeName) {
		int counter = defaultTableModel.getRowCount()+1;
		videoIdIndexMap.put(counter, video);
		if (getLastSelectedTreeNode.equals(nodeName)) {
			String[] row = {String.valueOf(counter), video.getVideoTitle(), video.getVideoDescription()};
			defaultTableModel.addRow(row);
		}		
	}
	
	
	private void createInformationTree() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("VidBox");
		defaultTreeModel = new DefaultTreeModel(root);
		DefaultMutableTreeNode adminNode = new DefaultMutableTreeNode("Admin");
        DefaultMutableTreeNode memberNode = new DefaultMutableTreeNode("Member");
        DefaultMutableTreeNode invitationNode = new DefaultMutableTreeNode("Invitations");
        DefaultMutableTreeNode videosNode = new DefaultMutableTreeNode("Videos");
        DefaultMutableTreeNode myVideosNode = new DefaultMutableTreeNode("My Videos");
        DefaultMutableTreeNode sharedVideosNode = new DefaultMutableTreeNode("Shared Videos");
        DefaultMutableTreeNode publicVideosNode = new DefaultMutableTreeNode("Public Videos");
        root.add(adminNode);
        root.add(memberNode);
        root.add(invitationNode);
        root.add(videosNode);
        videosNode.add(publicVideosNode);
        videosNode.add(myVideosNode);
        videosNode.add(sharedVideosNode);
        informationTree = new JTree(defaultTreeModel);
        populateAdminNode();
        populateInvitationsNode();
        populateMemberNode();
        informationTree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
            	if (informationTree.getLastSelectedPathComponent() != null){
            		getLastSelectedTreeNode = informationTree.getLastSelectedPathComponent().toString();
            	}            	
            	peerShareButton.setEnabled(false);
            	groupShareButton.setEnabled(false);
            	sendInvitationButton.setEnabled(false);
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                		informationTree.getLastSelectedPathComponent();
                if (node != null) {
                	if (node.getParent().toString().equals("Invitations")) {
                		sendInvitationButton.setEnabled(false);
                		TreeNode parentNode = node.getParent();
                		System.out.println(parentNode.getIndex(node));
                		currentInvitationNodeIndex = parentNode.getIndex(node);
                		new GroupInvitationScreen(groupInvitationIdMap.get(parentNode.getIndex(node)));
                		HomeScreen.this.setEnabled(false);
                	} else if (node.getParent().toString().equals("Admin")) {
                		sendInvitationButton.setEnabled(true);
                		TreeNode parentNode = node.getParent();
                		currentSelectedAdminGroup = adminGroupIdMap.get(parentNode.getIndex(node));
                		ArrayList<Video> videoList = VidBoxHttpClient.getInstance().fetchGroupVideos(currentSelectedAdminGroup.getGroupId());
                		populateVideoTable(videoList, node.toString());
                	} else if (node.toString().equals("My Videos")) {
                		peerShareButton.setEnabled(true);
                		groupShareButton.setEnabled(true);
                		ArrayList<Video> videoList = VidBoxHttpClient.getInstance().fetchMyVideos();
                		populateVideoTable(videoList, "My Videos");
                	} else if (node.toString().equals("Public Videos")) {
                		ArrayList<Video> videoList = VidBoxHttpClient.getInstance().fetchPublicVideos();
                		populateVideoTable(videoList, "Public Videos");
                	} else if (node.toString().equals("Shared Videos")) {
                		ArrayList<Video> videoList = VidBoxHttpClient.getInstance().fetchSharedVideos();
                		populateVideoTable(videoList, "Shared Videos");           		
                	} else if (node.getParent().toString().equals("Member")) {
                		TreeNode parentNode = node.getParent();
                		int nodeIndex = parentNode.getIndex(node);
                		int groupId = memberGroupIdMap.get(nodeIndex).getGroupId();
                		ArrayList<Video> videoList = VidBoxHttpClient.getInstance().fetchGroupVideos(groupId);
                		populateVideoTable(videoList, node.toString());                		
                	}
                }

            }
        });
    
	}
	
	private void createGroupButton() {
		createGroupButton = new JButton("Create Group");
		createGroupButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				new CreateGroupScreen();
				
			}
		});
	}
	
	public int addNode(String groupName, String nodeName) {
		int index = 0;
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) defaultTreeModel.getRoot();
		Enumeration children = root.children();
        while(children.hasMoreElements()){
        	DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
            if(node.toString().equals(nodeName)){
            	DefaultMutableTreeNode groupNode = new DefaultMutableTreeNode(groupName);
            	node.add(groupNode);
            	index = node.getIndex(groupNode);
            	informationTree.repaint();
            	defaultTreeModel.reload(root);
            }
        }
        return index;
	}
	
	private void populateAdminNode() {
		ArrayList<Group> adminGroupList = loginResponse.getAdminGroup();
		for (int index = 0; index < adminGroupList.size(); index++) {
			int nodeIndex = addNode(adminGroupList.get(index).getGroupName(), "Admin");
			adminGroupIdMap.put(nodeIndex, adminGroupList.get(index));
			openGroupList.add(adminGroupList.get(index));
		}
	}
	
	private void populateMemberNode() {
		ArrayList<Group> memberGroupList = loginResponse.getMemberGroup();
		for (int index = 0; index < memberGroupList.size(); index++) {
			int nodeIndex =  addNode(memberGroupList.get(index).getGroupName(), "Member");
			memberGroupIdMap.put(nodeIndex, memberGroupList.get(index));
			if (memberGroupList.get(index).getGroupType().equals("Open")) {
				openGroupList.add(memberGroupList.get(index));
			}
		}
	}
	
	private void populateInvitationsNode() {
		ArrayList<GroupInvitation> invitationList = loginResponse.getInvitationList();
		for (int index = 0; index < invitationList.size(); index++) {
			int nodeIndex = addNode(invitationList.get(index).getGroup().getGroupName(), "Invitations");
			groupInvitationIdMap.put(nodeIndex, invitationList.get(index));
		}
	}
	
	public void refreshInvitationNode() {
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)defaultTreeModel.getRoot();
		Enumeration children = rootNode.children();
        while(children.hasMoreElements()){
        	DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
        	if(node.toString().equals("Invitations")){
        		node.removeAllChildren();
        		defaultTreeModel.reload(rootNode);
        		informationTree.repaint();
        		groupInvitationIdMap.remove(currentInvitationNodeIndex);
        		currentInvitationNodeIndex = -1;
        		ArrayList<GroupInvitation> groupInvitationList = new ArrayList<GroupInvitation>(groupInvitationIdMap.values());
        		groupInvitationIdMap.clear();
        		for (int index = 0; index < groupInvitationList.size(); index++) {
        			int nodeIndex = addNode(groupInvitationList.get(index).getGroup().getGroupName(), "Invitations");
        			groupInvitationIdMap.put(nodeIndex, groupInvitationList.get(index));
        		}
        		
        	} 
        }
	}
	
	private void createSendInvitationButton() {
		sendInvitationButton = new JButton("Send Invitation");
		sendInvitationButton.setEnabled(false);
		sendInvitationButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new SendGroupInvitationScreen(currentSelectedAdminGroup);
			}
		});
	}
	
	public void addMemberNode(GroupInvitation groupInvitation) {
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)defaultTreeModel.getRoot();
		Enumeration children = rootNode.children();
		 while(children.hasMoreElements()){
			 DefaultMutableTreeNode node = (DefaultMutableTreeNode) children.nextElement();
			 if(node.toString().equals("Member")){
				 int index = addNode(groupInvitation.getGroup().getGroupName(), "Member");
				 memberGroupIdMap.put(index, groupInvitation.getGroup());
				 if (groupInvitation.getGroup().getGroupType().equals("Open")) {
					 openGroupList.add(groupInvitation.getGroup());
				 }
			 }
		 }
	}
	
	private void createGroupShareButton() {
		groupShareButton = new JButton(" Group Share");
		groupShareButton.setEnabled(false);
		groupShareButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(videoListTable.getSelectedRow());
				if (videoListTable.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(HomeScreen.this, "Please select a video to share");
				} else {
					int index = Integer.parseInt(videoListTable.getModel().getValueAt(videoListTable.getSelectedRow(), 0).toString());
					System.out.println("Fetching from videoIdIndexMap is "+videoIdIndexMap.get(index).getVideoId());
					new VideoGroupShareScreen(openGroupList, videoIdIndexMap.get(index));
				}		
			}
			
		});
	}
	
	private void createPeerShareButton() {
		peerShareButton = new JButton("Peer Share");
		peerShareButton.setEnabled(false);
		peerShareButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(videoListTable.getSelectedRow());
				if (videoListTable.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(HomeScreen.this, "Please select a video to share");
				} else {
					int index = Integer.parseInt(videoListTable.getModel().getValueAt(videoListTable.getSelectedRow(), 0).toString());
					System.out.println("Fetching from videoIdIndexMap is "+videoIdIndexMap.get(index).getVideoId());
					new VideoPeerShareScreen(videoIdIndexMap.get(index));
				}
							
			}
		});
	}
	
	private void populateVideoTable(ArrayList<Video> videoList, String nodeName) {
		int counter = 0;
		while(defaultTableModel.getRowCount() > 0)
		{
			defaultTableModel.removeRow(0);
		}
		videoIdIndexMap.clear();
		for (int index = 0; index < videoList.size(); index++) {
			counter++;
			Video video = videoList.get(index);
			if (getLastSelectedTreeNode.equals(nodeName)) {
				String[] row = {String.valueOf(counter), video.getVideoTitle(), video.getVideoDescription()};
				defaultTableModel.addRow(row);
			}		
			videoIdIndexMap.put(counter, video);
		}
	}
	
	private void createJoinGroupButton() {
		joinGroupButton = new JButton("Join Group");
		joinGroupButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JoinPublicGroupScreen.getInstance();
				
			}
		});
	}

}
