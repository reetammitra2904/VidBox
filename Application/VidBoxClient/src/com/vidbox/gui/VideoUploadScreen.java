package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Video;

public class VideoUploadScreen extends JFrame{
	
	private JLabel videoFileLabel;
	private JButton selectButton;
	private JTextField fileLocation;
	private JLabel videoSubjectLabel;
	private JTextField videoSubject;
	private JLabel videoDescriptionLabel;
	private JTextArea videoDescription;
	private JComboBox<String> videoCategory;
	private JComboBox<String> viewLevel;
	private JLabel viewLevelLabel;
	private JLabel videoCategoryLabel;
	private JButton uploadButton;
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private File videoFile;
	
	
	public VideoUploadScreen() {
		init();
	}
	
	private void init() {
		
		videoFileLabel = new JLabel("Video:");
		fileLocation = new JTextField(20);
		videoSubjectLabel = new JLabel("Subject:");
		videoSubject = new JTextField(20);
		videoDescriptionLabel = new JLabel("Description:");
		videoDescription = new JTextArea(10, 20);
		String[] videoCategoryArray = {"Technical", "Entertainment"};
		videoCategory = new JComboBox<String>();
		videoCategory.addItem(videoCategoryArray[0]);
		videoCategory.addItem(videoCategoryArray[1]);
		String[] viewLevelArray = {"Public", "Private"};
		videoCategoryLabel = new JLabel("Category");
		viewLevel = new JComboBox<String>();
		viewLevel.addItem(viewLevelArray[0]);
		viewLevel.addItem(viewLevelArray[1]);
		viewLevelLabel = new JLabel("View Level:");
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		createSelectButton();
		createUploadButton();
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(videoFileLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(fileLocation, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 2;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(selectButton, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(viewLevelLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(viewLevel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(videoCategoryLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(videoCategory, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(videoSubjectLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(videoSubject, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(videoDescriptionLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(videoDescription, mainPanelConstraints);
		
		buttonPanel.add(uploadButton);
		
        getContentPane().add(mainPanel, new BorderLayout().CENTER);
        getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width;
		setSize(width/2, height/2);
		setLocationRelativeTo(null);
		setTitle("Home");
		setVisible(true);
		
		
		
		
	}
	
	private void createSelectButton() {
		selectButton = new JButton("Select");
		selectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser videoFileChooser = new JFileChooser();
				int returnVal=videoFileChooser.showOpenDialog(VideoUploadScreen.this);
				  if (returnVal == JFileChooser.APPROVE_OPTION) {
					  fileLocation.setText(videoFileChooser.getSelectedFile().getAbsolutePath());
					  videoFile = videoFileChooser.getSelectedFile();
				  }
			}
			
		});
	}
	
	private void createUploadButton() {
		uploadButton = new JButton("Upload");
		uploadButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String videoDate = Calendar.getInstance().getTime().toString();
				String videoId = VidBoxHttpClient.getInstance().uploadVideoFile(videoFile,viewLevel.getSelectedItem().toString(), videoCategory.getSelectedItem().toString()
						, videoSubject.getText(), videoDescription.getText(), videoDate);
				if (!videoId.equals("")) {
					System.out.println("The video id is "+videoId);
					Video video = new Video();
					video.setCategory(videoCategory.getSelectedItem().toString());
					video.setDate(videoDate);
					video.setUsername(HomeScreen.getInstance().username);
					video.setVideoDescription(videoDescription.getText());
					video.setVideoId(videoId);
					video.setVideoTitle(videoSubject.getText());
					video.setViewLevel(viewLevel.getSelectedItem().toString());
					JOptionPane.showMessageDialog(VideoUploadScreen.this, "Congo!! Your video was successfully uploaded!!!");
					HomeScreen.getInstance().refreshListTable(video, "My Videos");
					dispatchEvent(new WindowEvent(VideoUploadScreen.this, WindowEvent.WINDOW_CLOSING));
				} else {
					JOptionPane.showMessageDialog(VideoUploadScreen.this, "Sorry!! Your video couldn't be uploaded!! :-(");
				}
				
			}
			
		});
	}
	
	

}
