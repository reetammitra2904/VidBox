package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Group;
import com.vidbox.model.GroupInvitation;

public class GroupInvitationScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel groupName;
	private JLabel groupDescription;
	private JLabel groupAdmin;
	private ButtonGroup buttonGroup;
	private JRadioButton yesButton;
	private JRadioButton noButton;
	private GroupInvitation groupInvitation;
	private JButton submitButton;
	private JButton cancelButton;
	
	public GroupInvitationScreen(GroupInvitation groupInvitation) {
		this.groupInvitation = groupInvitation;
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		groupName = new JLabel("Group Name: "+groupInvitation.getGroup().getGroupName());
		groupDescription = new JLabel("Group Description: "+groupInvitation.getGroup().getGroupDescription());
		groupAdmin = new JLabel("Group Admin: "+groupInvitation.getGroup().getAdmin());
		yesButton = new JRadioButton("Yes", true);
		noButton = new JRadioButton("No", false);
		buttonGroup = new ButtonGroup();
		buttonGroup.add(yesButton);
		buttonGroup.add(noButton);
		createSubmitButton();
		createCancelButton();
		
		buttonPanel.add(submitButton);
		buttonPanel.add(cancelButton);
		
		mainPanel.setLayout(new GridBagLayout());
	    GridBagConstraints mainPanelConstraints = new GridBagConstraints();
	    mainPanelConstraints.insets = new Insets(10,10,0,0);
	    
	    mainPanelConstraints.gridx = 0;
	    mainPanelConstraints.gridy = 0;
	    mainPanelConstraints.gridwidth = 2;
	    mainPanel.add(groupName, mainPanelConstraints);
	    
	    mainPanelConstraints.gridx = 0;
	    mainPanelConstraints.gridy = 1;
	    mainPanelConstraints.gridwidth = 2;
	    mainPanel.add(groupAdmin, mainPanelConstraints);
	    
	    mainPanelConstraints.gridx = 0;
	    mainPanelConstraints.gridy = 2;
	    mainPanelConstraints.gridwidth = 2;
	    mainPanel.add(groupDescription, mainPanelConstraints);
	    
	    mainPanelConstraints.gridwidth = 1;
	    
	    mainPanelConstraints.gridx = 0;
	    mainPanelConstraints.gridy = 3;
	    mainPanel.add(yesButton, mainPanelConstraints);
	    
	    mainPanelConstraints.gridx = 1;
	    mainPanelConstraints.gridy = 3;
	    mainPanel.add(noButton, mainPanelConstraints);
	    
	    getContentPane().add(mainPanel, new BorderLayout().CENTER);
	    getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
	    setTitle(groupInvitation.getGroup().getGroupName()+" Group Invitation");
	    setSize(400,400);
	    setLocationRelativeTo(null);
	    setVisible(true);
	    
	    
	}
	
	private void createSubmitButton() {
		submitButton = new JButton("Submit");
		submitButton.addActionListener(new ActionListener() {

			@Override
			
			public void actionPerformed(ActionEvent e) {
				boolean isAccept = yesButton.isSelected();
				boolean result = VidBoxHttpClient.getInstance().respondGroupInvitation(groupInvitation.getGroupInvitationId(), isAccept, groupInvitation.getGroup().getGroupId());
				if (result) {
					HomeScreen.getInstance().setEnabled(true);
					JOptionPane.showMessageDialog(GroupInvitationScreen.this, "You are now a memeber of "+groupInvitation.getGroup().getGroupName());
					HomeScreen.getInstance().refreshInvitationNode();
					HomeScreen.getInstance().addMemberNode(groupInvitation);
					GroupInvitationScreen.this.dispose();
				}
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				GroupInvitationScreen.this.dispose();
				
			}
			
		});
	}
	
	
	

}
