package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Video;

public class VideoPeerShareScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel userLabel;
	private JTextField user;
	private JButton shareButton;
	private JButton cancelButton;
	private Video video;
	
	public VideoPeerShareScreen(Video video) {
		this.video = video;
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		userLabel = new JLabel("User:");
		user = new JTextField(15);
		createShareButton();
		createCancelButton();
		
		mainPanel.add(userLabel);
		mainPanel.add(user);
		buttonPanel.add(shareButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		setTitle("Share "+video.getVideoTitle());
		setSize(400,400);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	private void createShareButton() {
		shareButton = new JButton("Share");
		shareButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean result = VidBoxHttpClient.getInstance().shareVideoPeer(user.getText(), video.getVideoId());
				if (result) {
					JOptionPane.showMessageDialog(VideoPeerShareScreen.this, "Your video has been shared with "+user.getText());
					VideoPeerShareScreen.this.dispose();
				} else {
					JOptionPane.showMessageDialog(VideoPeerShareScreen.this, "This user doesn't exist");
				}
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			VideoPeerShareScreen.this.dispose();
				
			}
		});
	}
	

}
