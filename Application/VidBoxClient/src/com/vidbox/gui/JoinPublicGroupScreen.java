package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class JoinPublicGroupScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JTable groupTable;
	private DefaultTableModel groupTableModel;
	private JButton joinButton;
	private JScrollPane scrollPane;
	private JPanel tablePanel;
	
	private static JoinPublicGroupScreen joinPublicGroupScreen = null;
	
	private JoinPublicGroupScreen() {
		init();
	}
	
	public static JoinPublicGroupScreen getInstance() {
		if (joinPublicGroupScreen == null) {
			joinPublicGroupScreen = new JoinPublicGroupScreen();
		}
		return joinPublicGroupScreen;
	}
	
	public void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		tablePanel = new JPanel();
		searchLabel = new JLabel("Search Group");
		search = new JTextField(20);
		createGroupTable();
		createJoinButton();
		scrollPane = new JScrollPane(groupTable);
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(searchLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(search, mainPanelConstraints);
		

		
		tablePanel.add(scrollPane);
		
		buttonPanel.add(joinButton);
		
		getContentPane().add(mainPanel, new BorderLayout().NORTH);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		getContentPane().add(tablePanel, new BorderLayout().CENTER);
		
		setSize(500, 400);
		setLocationRelativeTo(null);
		setVisible(true);
		setTitle("Join Group");
		
		
	}
	
	private void createGroupTable() {
		groupTableModel = new DefaultTableModel();
		groupTableModel.addColumn("S.No.");
		groupTableModel.addColumn("Group Name");
		groupTableModel.addColumn("Group Description");
		groupTable = new JTable(groupTableModel);
	}
	
	private void createJoinButton() {
		joinButton = new JButton("Join");
		joinButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	

}
