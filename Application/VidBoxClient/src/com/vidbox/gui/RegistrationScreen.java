package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import com.vidbox.httpclient.VidBoxHttpClient;

public class RegistrationScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel nameLabel;
	private JTextField name;
	private JLabel usernameLabel;
	private JTextField username;
	private JLabel passwordLabel;
	private JPasswordField password;
	private JLabel confirmPasswordLabel;
	private JPasswordField confirmPassword;
	private JLabel emailAddressLabel;
	private JTextField emailAddress;
	private JButton registerButton;
	private JButton cancelButton;
	
	private static RegistrationScreen registrationScreen = null;
	
	public static RegistrationScreen getInstance() {
		if (registrationScreen == null) {
			registrationScreen = new RegistrationScreen();
		}
		
		return registrationScreen;
	}
	
	private RegistrationScreen() {
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		nameLabel = new JLabel("Name");
		name = new JTextField(20);
		usernameLabel = new JLabel("Username");
		username = new JTextField(20);
		username.addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (!username.getText().trim().equals("")) {
					boolean result = VidBoxHttpClient.getInstance().checkUsername(username.getText());
					if (!result) {
						JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "This username is already taken!!");
						username.setText("");
					}
				}
				
			}
			
		});
		passwordLabel = new JLabel("Password");
		password = new JPasswordField(20);
		confirmPasswordLabel = new JLabel("Confirm Password");
		confirmPassword = new JPasswordField(20);
		emailAddressLabel = new JLabel("Email Address");
		emailAddress = new JTextField(20);
		createRegisterButton();
		createCancelButton();
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(nameLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(name, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(usernameLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(username, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(passwordLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(password, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(confirmPasswordLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(confirmPassword, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(emailAddressLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(emailAddress, mainPanelConstraints);
		
		buttonPanel.add(registerButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width;
		setSize(width/2, height/2);
		setLocationRelativeTo(null);
		setTitle("Registration");
		setVisible(true);
		
		
	}
	
	private void createRegisterButton() {
		registerButton = new JButton("Register");
		registerButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (name.getText().trim().equals("") || username.getText().trim().equals("") ||
						password.getText().trim().equals("") || emailAddress.getText().trim().equals("")) {
				JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "Please enter all the fields!!!");	
				}else if (!password.getText().equals(confirmPassword.getText())) {
                JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "Your passwords do not match!!!");
				} else {
					boolean result = VidBoxHttpClient.getInstance().registerUser(name.getText(), 
							username.getText(), password.getText(), emailAddress.getText());
					if (result) {
						JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "You are good to go!!!");
						RegistrationScreen.getInstance().setVisible(false);
						LoginScreen.getInstance().setVisible(true);
					} else {
						JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "Sorry, You haven't been registered. Please try again!!!");
					}
				}
				
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				RegistrationScreen.getInstance().setVisible(false);
				MainScreen.getInstance().setVisible(true);
				
			}
			
		});
	}
	
	

}
