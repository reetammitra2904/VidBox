package com.vidbox.gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JButton signInButton;
	private JButton registerButton;
	private JLabel welcomeLabel;
	
	private static MainScreen mainScreen = null;
	
	private MainScreen() {
		init();
	}
	
	public static MainScreen getInstance() {
		if (mainScreen == null) {
			mainScreen = new MainScreen();
		}
		return mainScreen;
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		welcomeLabel = new JLabel("<html>  Welcome <br> to VidBox!!!</html>");
		createSignInButton();
		createRegisterButton();	
		welcomeLabel.setFont (welcomeLabel.getFont ().deriveFont (64.0f));
		mainPanel.add(welcomeLabel, new BorderLayout().CENTER);	
		buttonPanel.add(signInButton);
		buttonPanel.add(registerButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width;
		setSize(width/2, height/2);
		setLocationRelativeTo(null);
		setTitle("VidBox");
		setVisible(true);
	}
	
	private void createSignInButton() {
		signInButton = new JButton("Sign In");
		signInButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (LoginScreen.getInstance() != null) {
					LoginScreen.getInstance().setVisible(true);
				} else {
					LoginScreen.getInstance();
				}
				
				MainScreen.getInstance().setVisible(false);
				
			}
			
		});
	}
	
	private void createRegisterButton() {
		registerButton = new JButton("Register");
		registerButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
	              if (RegistrationScreen.getInstance() != null) {
	            	  RegistrationScreen.getInstance().setVisible(true);
	              } else {
	            	  RegistrationScreen.getInstance();
	              }
	              MainScreen.getInstance().setVisible(false);
			}
			
		});
	}
	
	

}
