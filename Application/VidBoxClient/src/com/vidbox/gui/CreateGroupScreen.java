package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.Group;

public class CreateGroupScreen extends JFrame{
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel groupNameLabel;
	private JTextField groupName;
	private JLabel groupTypeLabel;
	private JComboBox<String> groupType;
	private JLabel groupCategoryLabel;
	private JComboBox<String> groupCategory;
	private JLabel groupShareLevelLabel;
	private JComboBox<String> groupShareLevel;
	private JLabel groupDescriptionLabel;
	private JTextArea groupDescription;
	private JButton createGroupButton;
	private JButton cancelButton;
	
	public CreateGroupScreen() {
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		groupNameLabel = new JLabel("Group Name:");
		groupName = new JTextField(20);
		groupTypeLabel = new JLabel("Group Type:");
		String[] groupTypeArray = {"Open", "Closed"};
		groupType = new JComboBox<String>();
		groupType.addItem(groupTypeArray[0]);
		groupType.addItem(groupTypeArray[1]);
		groupCategoryLabel = new JLabel("Group Category:");
		String[] groupCategoryArray = {"Entertainment", "Educational"};
		groupCategory = new JComboBox<String>();
		groupCategory.addItem(groupCategoryArray[0]);
		groupCategory.addItem(groupCategoryArray[1]);
		groupShareLevelLabel = new JLabel("Group Share Level:");
		String[] groupShareLevelArray = {"Public", "Private"};
		groupShareLevel = new JComboBox<String>();
		groupShareLevel.addItem(groupShareLevelArray[0]);
		groupShareLevel.addItem(groupShareLevelArray[1]);
		groupDescriptionLabel = new JLabel("Group Description:");
		groupDescription = new JTextArea(10, 20);
		
		createCancelButton();
		createCreateGroupButton();
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(groupNameLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(groupName, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(groupTypeLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(groupType, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(groupShareLevelLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 2;
		mainPanel.add(groupShareLevel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(groupCategoryLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 3;
		mainPanel.add(groupCategory, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(groupDescriptionLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 4;
		mainPanel.add(groupDescription, mainPanelConstraints);
		
		buttonPanel.add(createGroupButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		setSize(400, 400);
		setTitle("Create Group");
		setVisible(true);
		setLocationRelativeTo(null);
		
	}
	
	private void createCreateGroupButton() {
		createGroupButton = new JButton("Create Group");
		createGroupButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Group group = new Group();
				group.setGroupName(groupName.getText());
				group.setGroupCategory(groupCategory.getSelectedItem().toString());
				group.setGroupShareLevel(groupShareLevel.getSelectedItem().toString());
				group.setGroupType(groupType.getSelectedItem().toString());
				group.setGroupDescription(groupDescription.getText());
				group.setAdmin(HomeScreen.getInstance().username);
				int groupId = VidBoxHttpClient.getInstance().createGroup(group);
				if (groupId != 0) {
					JOptionPane.showMessageDialog(CreateGroupScreen.this, "Your group was created successfully");
					group.setGroupId(groupId);
					int nodeIndex = HomeScreen.getInstance().addNode(groupName.getText(), "Admin");
					HomeScreen.getInstance().adminGroupIdMap.put(nodeIndex, group);
					HomeScreen.getInstance().openGroupList.add(group);
					CreateGroupScreen.this.dispose();
				}
				
			}
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				CreateGroupScreen.this.dispose();
			}
		});
	}
	
	

}
