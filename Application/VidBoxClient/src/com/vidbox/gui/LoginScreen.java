package com.vidbox.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.vidbox.httpclient.VidBoxHttpClient;
import com.vidbox.model.LoginResponse;
import com.vidbox.model.Video;
import com.vidbox.model.VideoList;

public class LoginScreen extends JFrame{ 
	
	private JPanel mainPanel;
	private JPanel buttonPanel;
	private JLabel usernameLabel;
	private JTextField username;
	private JLabel passwordLabel;
	private JPasswordField password;
	private JButton loginButton;
	private JButton cancelButton;
	
	private static LoginScreen loginScreen = null;
	
	public static LoginScreen getInstance() {
		if (loginScreen == null) {
			loginScreen = new LoginScreen();
		}
		return loginScreen;
	}
	
	private LoginScreen() {
		init();
	}
	
	private void init() {
		mainPanel = new JPanel();
		buttonPanel = new JPanel();
		usernameLabel = new JLabel("Username");
		username = new JTextField(20);
		passwordLabel = new JLabel("Password");
		password = new JPasswordField(20);
		createLoginButton();
		createCancelButton();
		
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints mainPanelConstraints = new GridBagConstraints();
		mainPanelConstraints.insets = new Insets(10,10,0,0);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(usernameLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 0;
		mainPanel.add(username, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 0;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(passwordLabel, mainPanelConstraints);
		
		mainPanelConstraints.gridx = 1;
		mainPanelConstraints.gridy = 1;
		mainPanel.add(password, mainPanelConstraints);
		
		buttonPanel.add(loginButton);
		buttonPanel.add(cancelButton);
		
		getContentPane().add(mainPanel, new BorderLayout().CENTER);
		getContentPane().add(buttonPanel, new BorderLayout().SOUTH);
		
		
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width;
		setSize(width/2, height/2);
		setLocationRelativeTo(null);
		setTitle("Login");
		setVisible(true);
		
		
	}
	
	private void createLoginButton() {
		loginButton = new JButton("Login");
		loginButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (username.getText().trim().equals("") || password.getText().trim().equals("")) {
					JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "Please enter both username and password!!!");
				} else {
					LoginResponse loginResponse = VidBoxHttpClient.getInstance().loginUser
							(username.getText(), password.getText());
					if (loginResponse != null) {
						HomeScreen.getInstance(username.getText(), loginResponse);
						LoginScreen.getInstance().setVisible(false);
					} else {
						JOptionPane.showMessageDialog(RegistrationScreen.getInstance(), "Please enter valid username and password!!");
					}
				}
				
				
			}
			
		});
	}
	
	private void createCancelButton() {
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MainScreen.getInstance().setVisible(true);
				LoginScreen.getInstance().setVisible(false);
				
			}
			
		});
	}
	
	
	
	
	

}
