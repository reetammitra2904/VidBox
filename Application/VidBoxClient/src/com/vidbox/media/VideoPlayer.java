package com.vidbox.media;


import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.io.IOUtils;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.EmbeddedMediaPlayer;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;





public class VideoPlayer implements Runnable{
	
	private JButton stopButton;
	private JButton pauseButton;
	private EmbeddedMediaPlayer mediaPlayer;
	private InputStream inputStream;
	private JSlider slider;
	private JPanel buttonPanel;
	
	

    public  VideoPlayer(InputStream inputStream) {
    	this.inputStream = inputStream;
       
    }
    
    private void init() {
    	NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), "/usr/bin/vlc/");
        Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);
    	JFrame frame = new JFrame("Video");
        createPauseButton();
        createStopButton();
        createSlider();
        
        buttonPanel = new JPanel();
        
        buttonPanel.add(pauseButton);
        buttonPanel.add(stopButton);
        buttonPanel.add(slider);
        
        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
       
        Canvas c = new Canvas();
        c.setBackground(Color.black);
        
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(c, BorderLayout.CENTER);
        frame.add(p, BorderLayout.CENTER);
        frame.add(buttonPanel, BorderLayout.SOUTH);


        mediaPlayer = mediaPlayerFactory.newEmbeddedMediaPlayer();
        mediaPlayer.setEnableMouseInputHandling(false);
        mediaPlayer.setEnableKeyInputHandling(false);
        c.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("Mouse clicked");
				if (e.getClickCount()==1) {
					System.out.println(mediaPlayer.getLength());
				}
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        mediaPlayer.setVideoSurface(mediaPlayerFactory.newVideoSurface(c));
        frame.setLocation(100, 100);
        frame.setSize(1050, 600);
        frame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosing(WindowEvent e) {
				mediaPlayer.stop();
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
        	
        });
        frame.setVisible(true);

        File videoFile;
		try {
			videoFile = File.createTempFile("Video", "");
			OutputStream outputStream = new FileOutputStream(videoFile);
			IOUtils.copy(inputStream, outputStream);
			mediaPlayer.playMedia(videoFile.getAbsolutePath());
			outputStream.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
      
        
        
    }
    
    private void createStopButton() {
    	stopButton = new JButton("Stop");
    	stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mediaPlayer.stop();
				stopButton.setEnabled(false);
				slider.setValue(0);
				pauseButton.setText("Play");
			}
    		
    	});
    }
    
    private void createPauseButton() {
    	pauseButton = new JButton("Pause");
    	pauseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (mediaPlayer.isPlaying()) {
					mediaPlayer.pause();
					pauseButton.setText("Play");
				} else {
					mediaPlayer.play();
					pauseButton.setText("Pause");
				}
				
			}
    		
    	});
    }

	@Override
	public void run() {
		 init();
		
	}
	
	private void createSlider() {
		slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 0);

	    slider.setMinorTickSpacing(2);
	    slider.setMajorTickSpacing(10);
	    slider.setPaintTicks(true);
	    slider.setPaintLabels(true);

	    // We'll just use the standard numeric labels for now...
	    slider.setLabelTable(slider.createStandardLabels(10));
	    slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				long sliderValue = slider.getValue();
				System.out.println(slider.getValue());
				System.out.println(mediaPlayer.getLength());
				System.out.println(sliderValue*mediaPlayer.getLength()/100);
				mediaPlayer.setTime(sliderValue*mediaPlayer.getLength()/100);
				
			}
		});

	}
	
	public static void main(String[] args) {
		InputStream inputStream;
		try {
			inputStream = new FileInputStream(new File("/home/reets/Videos/TestVideo.mp4"));
			Thread javaFXPlayerThread = new Thread(new VideoPlayer(inputStream));
			javaFXPlayerThread.start();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
	
