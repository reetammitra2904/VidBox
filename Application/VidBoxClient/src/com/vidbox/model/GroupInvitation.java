package com.vidbox.model;

public class GroupInvitation {
	
	private String user;
	private String invitationMessage;
	private Group group;
	private int groupInvitationId;
	
	
	
	public int getGroupInvitationId() {
		return groupInvitationId;
	}
	public void setGroupInvitationId(int groupInvitationId) {
		this.groupInvitationId = groupInvitationId;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getInvitationMessage() {
		return invitationMessage;
	}
	public void setInvitationMessage(String invitationMessage) {
		this.invitationMessage = invitationMessage;
	}
	
	

}
