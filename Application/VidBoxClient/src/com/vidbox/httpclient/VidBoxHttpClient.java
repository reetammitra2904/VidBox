package com.vidbox.httpclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.vidbox.media.VideoPlayer;
import com.vidbox.model.Group;
import com.vidbox.model.LoginResponse;
import com.vidbox.model.Video;
import com.vidbox.model.VideoList;
import com.vidbox.gui.HomeScreen;


public class VidBoxHttpClient {

	private  HttpClient client = null;
	
	private static VidBoxHttpClient vidBoxHttpClient = null;
	
	private VidBoxHttpClient() {
		if (client == null) {
			client = new HttpClient();
		}
		
	}
	
	public static VidBoxHttpClient getInstance() {
		if (vidBoxHttpClient == null) {
			vidBoxHttpClient = new VidBoxHttpClient();
		}
		
		return vidBoxHttpClient;
	}
	
	public  LoginResponse loginUser(String username, String password) {
		PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/login");
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
	    postParameters.add(new BasicNameValuePair("username", username));
	    postParameters.add(new BasicNameValuePair("password", password));

	    try {
			request.addParameter("username", username);
			request.addParameter("password", password);
			int responseCode = client.executeMethod(request);
			if (responseCode == 200) {
				ObjectMapper objectMapper = new ObjectMapper();
				LoginResponse loginResponse = objectMapper.readValue(request.getResponseHeader("loginResponse").getValue(), LoginResponse.class);
				return loginResponse;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return null;
	}
	
	public boolean registerUser(String name, String username, String password, 
			String emailAddress) {
		PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/register");
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
	    postParameters.add(new BasicNameValuePair("username", username));
	    postParameters.add(new BasicNameValuePair("password", password));
	    postParameters.add(new BasicNameValuePair("name", name));
	    postParameters.add(new BasicNameValuePair("emailAddress", emailAddress));
	    try {
			request.addParameter("username", username);
			request.addParameter("password", password);
			request.addParameter("name", name);
			request.addParameter("emailAddress", emailAddress);
			int responseCode = client.executeMethod(request);
			//EntityUtils.consumeQuietly(response.getEntity());
			if (responseCode == 200) {
				return true;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return false;
	}
	
	public boolean checkUsername(String username) {
		GetMethod request = new GetMethod("http://localhost:8080/VidBoxServer/register");
	    try {
			request.addRequestHeader("username", username);
			int responseCode = client.executeMethod(request);
			//EntityUtils.consumeQuietly(response.getEntity());
			if (responseCode == 200) {
				return true;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return false;
		
	}
	
	public String uploadVideoFile(File videoFile, String viewLevel, String videoCategory, String videoSubject, 
			String videoDescription, String videoDate) {
		String videoId = "";
		PostMethod filePost = new PostMethod("http://localhost:8080/VidBoxServer/videoUpload");
		filePost.addRequestHeader("username", HomeScreen.username);
		filePost.addRequestHeader("viewLevel", viewLevel);
		filePost.addRequestHeader("videoCategory", videoCategory);
		filePost.addRequestHeader("videoSubject", videoSubject);
		filePost.addRequestHeader("videoDescription", videoDescription);
		filePost.addRequestHeader("videoDate", videoDate);
		System.out.println("The username is "+HomeScreen.username);
		try {	 
		  Part[] parts = {
			      new StringPart("param_name", "value"),
			      new FilePart(videoFile.getName(), videoFile)
			  };
		  filePost.setRequestEntity(
		      new MultipartRequestEntity(parts, filePost.getParams())
		      );
		  
		  client.executeMethod(filePost);
		  videoId = filePost.getResponseHeader("videoId").toString();
		} catch (FileNotFoundException e){// TODO Auto-generated catch block
			e.printStackTrace();} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return videoId;
	    
		}
	
	    public void fetchVideo(String videoId) {
	    	GetMethod request = new GetMethod("http://localhost:8080/VidBoxServer/fetchVideo");
	    	request.setRequestHeader("videoId", videoId);
	    	try {
	    		client.executeMethod(request);
				InputStream inputStream = request.getResponseBodyAsStream();
				Header videoName = request.getResponseHeader("videoName");
				//OutputStream outputStream = new FileOutputStream(new File("/home/reets/"+videoName.getValue()));
				//IOUtils.copy(inputStream, outputStream);
				//outputStream.close();
				Thread videoPlayerThread = new Thread(new VideoPlayer(inputStream));
				videoPlayerThread.start();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    public int createGroup(Group group) {
	    	ObjectMapper mapper = new ObjectMapper();
	    	try {
				String jsonInString = mapper.writeValueAsString(group);
				PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/createGroup");
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			    postParameters.add(new BasicNameValuePair("groupDetails", jsonInString));
			    request.addParameter("groupDetails", jsonInString);
			    int responseCode = client.executeMethod(request);
			    if (responseCode == 200) {
					return Integer.parseInt(request.getResponseHeader("groupId").getValue());
				}
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return 0;
	    	
	    }
	    
	    public boolean respondGroupInvitation(int groupInvitationId, boolean response, int groupId) {
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/invitationResponse");
	    	request.setParameter("response", String.valueOf(response));
	    	request.setParameter("groupInvitationId", String.valueOf(groupInvitationId));
	    	request.setParameter("groupId", String.valueOf(groupId));
	    	request.setParameter("username", HomeScreen.getInstance().username);
	    	 try {
				int responseCode = client.executeMethod(request);
				if (responseCode == 200) {
					return true;
				}
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return false;
	    }
	    
	    public boolean sendInvitation(String name, int groupId, String invitationMessage) {
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/sendInvitation");
	    	request.setParameter("user", name);
	    	request.setParameter("groupId", String.valueOf(groupId));
	    	request.setParameter("invitationMessage", invitationMessage);
	    	try {
				int responseCode = client.executeMethod(request);
				if (responseCode == 200) {
					return true;
				}
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    	return false;
	    }
	    
	    public boolean shareVideoPeer(String user, String videoId) {
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/sharePeerVideo");
	    	request.setParameter("user", HomeScreen.getInstance().username);
	    	request.setParameter("videoId", videoId);
	    	request.setParameter("peerUser", user);
	    	try {
				int responseCode = client.executeMethod(request);
				if (responseCode == 200) {
					return true;
				}
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return false;
	    }
	    
	    public boolean shareVideoGroup(int groupId, String videoId) {
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/shareGroupVideo");
	    	request.setParameter("user", HomeScreen.getInstance().username);
	    	request.setParameter("videoId", videoId);
	    	request.setParameter("groupId", String.valueOf(groupId));
	    	try {
				int responseCode = client.executeMethod(request);
				if (responseCode == 200) {
					return true;
				}
			} catch (HttpException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return false;
	    }
	    
	    public ArrayList<Video> fetchMyVideos() {
	    	VideoList videoList = null;
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/fetchMyVideos");
	    	request.setParameter("username", HomeScreen.getInstance().username);
	    	
	    	    try {
	    	    	int responseCode = client.executeMethod(request);
	    	    	if (responseCode == 200) {
	    	    		String jsonString = request.getResponseHeader("videoList").getValue();
	    	    		ObjectMapper objectMapper = new ObjectMapper();
					    videoList =  objectMapper.readValue(jsonString, VideoList.class);
					    return videoList.getVideoList();
	    	    	}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	return null;
	    }
	    
	    public ArrayList<Video> fetchSharedVideos() {
	    	VideoList videoList = null;
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/fetchSharedVideos");
	    	request.setParameter("username", HomeScreen.getInstance().username);
	    	
	    	    try {
	    	    	int responseCode = client.executeMethod(request);
	    	    	if (responseCode == 200) {
	    	    		String jsonString = request.getResponseHeader("videoList").getValue();
	    	    		ObjectMapper objectMapper = new ObjectMapper();
					    videoList =  objectMapper.readValue(jsonString, VideoList.class);
					    return videoList.getVideoList();
	    	    	}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	return null;
	    }
	    
	    public ArrayList<Video> fetchPublicVideos() {
	    	VideoList videoList = null;
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/fetchPublicVideos");
	    	request.setParameter("username", HomeScreen.getInstance().username);
	    	
	    	    try {
	    	    	int responseCode = client.executeMethod(request);
	    	    	if (responseCode == 200) {
	    	    		String jsonString = request.getResponseHeader("videoList").getValue();
	    	    		ObjectMapper objectMapper = new ObjectMapper();
					    videoList =  objectMapper.readValue(jsonString, VideoList.class);
					    return videoList.getVideoList();
	    	    	}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	return null;
	    }
	    
	    public ArrayList<Video> fetchGroupVideos(int groupId) {
	    	VideoList videoList = null;
	    	PostMethod request = new PostMethod("http://localhost:8080/VidBoxServer/fetchGroupVideos");
	    	request.setParameter("groupId", String.valueOf(groupId));
	    	
	    	    try {
	    	    	int responseCode = client.executeMethod(request);
	    	    	if (responseCode == 200) {
	    	    		String jsonString = request.getResponseHeader("videoList").getValue();
	    	    		ObjectMapper objectMapper = new ObjectMapper();
					    videoList =  objectMapper.readValue(jsonString, VideoList.class);
					    return videoList.getVideoList();
	    	    	}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	return null;
	    }
	}


	
	
	
	

