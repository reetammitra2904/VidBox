use vidbox;

create table user (
name varchar(100), 
username varchar(100), 
password varchar(100), 
emailAddress varchar(100), 
primary key(username)
);

create table group_t (
  groupId int not null AUTO_INCREMENT,
  adminId varchar(100) not null,
  groupName varchar(100),
  groupType varchar(100),
  groupCategory varchar(100),
  groupShareLevel varchar(100),
  groupDescription varchar(100),
  primary Key (groupId),
  foreign Key (adminId) references user (username)
  );

  create table join_t(
       groupId int not null,
       userId varchar(100) not null,
       dateInfo varchar(100),
       primary Key (groupId,userId),
       foreign Key (groupId) references group_t(groupId),
       foreign Key (userId) references user(username)
       );

create table video (
       videoId varchar(100), 
       username varchar(100), 
       title varchar(100), 
       description varchar(1000), 
       category varchar(100), 
       viewLevel varchar(100), 
       date varchar(100),
       primary key (videoId),
       foreign key (username) references user(username)
       );  

 create table groupShare(
       groupId int not null,
       userId varchar(100) not null,
       videoId varchar(100),
       primary Key(groupId,userId,videoId),
       foreign Key (groupId) references group_t(groupId),
       foreign Key(userId) references user(username),
       foreign Key(videoId) References video(videoId)
       );

  create table admin(
       adminId varchar(100) not null,
       groupId int not null,
       primary Key (adminId, groupId),
       foreign Key (adminId) references user(username),
       foreign Key (groupId) references group_t(groupID)
       );  

       create table groupInvitation(
       groupInvitationId int not null AUTO_INCREMENT,
       adminId varchar(100),
       groupId int,
       userId varchar(100),
       invitationMessage varchar(100),
       primary Key (groupInvitationId),
       foreign Key (adminId,groupId) references admin(adminId,groupId),
       foreign Key (userId) references user(username)
       );

       create table peerShare(
       peerShareId int not null AUTO_INCREMENT,
       userId varchar(100),
       pUserId varchar(100),
       videoId varchar(100),
       primary Key (peerShareId),
       foreign Key (userId) references user(username),
       foreign Key (pUserId) references user(username),
       foreign Key (videoId) references video(videoId)
       );  
