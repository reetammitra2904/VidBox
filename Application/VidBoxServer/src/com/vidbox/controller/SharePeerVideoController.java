package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vidbox.sql.SharePeerVideoSQL;

@WebServlet("/sharePeerVideo")
public class SharePeerVideoController extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		String user = request.getParameter("user");
		String videoId = request.getParameter("videoId");
		String peerUser = request.getParameter("peerUser");
		
		boolean result = SharePeerVideoSQL.getInstance().sharePeerVideo(user, peerUser, videoId);
		if (result) {
			response.setStatus(200);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	

}
