package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vidbox.sql.ShareGroupVideoSQL;

@WebServlet("/shareGroupVideo")
public class ShareGroupVideoController extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
		String username = request.getParameter("user");
		int groupId = Integer.parseInt(request.getParameter("groupId"));
		String videoId = request.getParameter("videoId");
		
		boolean result = ShareGroupVideoSQL.getInstance().shareGroupVideo(username, groupId, videoId);
		if (result) {
			response.setStatus(200);
		} else  {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	

}
