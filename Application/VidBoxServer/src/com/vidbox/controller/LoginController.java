package com.vidbox.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.vidbox.model.LoginResponse;
import com.vidbox.model.Video;
import com.vidbox.model.VideoList;
import com.vidbox.sql.FetchAdminGroupSQL;
import com.vidbox.sql.FetchGroupInvitationSQL;
import com.vidbox.sql.FetchMemberGroupSQL;
import com.vidbox.sql.FetchPublicVideosSQL;
import com.vidbox.sql.FetchVideoListSQL;
import com.vidbox.sql.LoginSQL;

@WebServlet("/login")
public class LoginController extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		System.out.println(username+" "+password);
		
		boolean result = LoginSQL.getInstance().loginUser(username, password);
		if (result) {
			LoginResponse loginResponse = new LoginResponse();		
			VideoList videoList = new VideoList();
			videoList.setVideoList(FetchPublicVideosSQL.getInstance().fetchVideoList());
			loginResponse.setVideoList(videoList);
			loginResponse.setAdminGroup(FetchAdminGroupSQL.getInstance().getAdminGroup(username));
			loginResponse.setMemberGroup(FetchMemberGroupSQL.getInstance().getMemberGroup(username));
			loginResponse.setInvitationList(FetchGroupInvitationSQL.getInstance().fetchGroupInvitations(username));
			ObjectMapper mapper = new ObjectMapper();
			String loginResponseJson = mapper.writeValueAsString(loginResponse);
			response.setHeader("loginResponse", loginResponseJson);
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
		
		
		
	}
}
