package com.vidbox.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.vidbox.mongodb.Fetch;
import com.vidbox.util.MongoDBConnection;

@WebServlet("/fetchVideo")
public class FetchVideoController extends HttpServlet{ 
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String videoId = request.getHeader("videoId");
		File video = Fetch.getInstance().getVideo(videoId);
		response.setHeader("videoName", video.getName());
		InputStream inputStream = new FileInputStream(video);
		OutputStream outputStream = response.getOutputStream();
		IOUtils.copy(inputStream, outputStream);
		outputStream.close();		
	}
	
	

}
