package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.vidbox.model.VideoList;
import com.vidbox.sql.FetchPublicVideosSQL;

@WebServlet("/fetchPublicVideos")
public class GetPublicVideosController extends HttpServlet{

	public void doPost(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
			VideoList videoList = new VideoList();
			videoList.setVideoList(FetchPublicVideosSQL.getInstance().fetchVideoList());
			ObjectMapper mapper = new ObjectMapper();
			String videoListJson = mapper.writeValueAsString(videoList);
			response.setHeader("videoList", videoListJson);
			response.setStatus(200);
			
		}
	
	

}
