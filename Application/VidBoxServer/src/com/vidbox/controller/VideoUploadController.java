package com.vidbox.controller;


import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.UploadContext;

import com.vidbox.sql.UploadSQL;

@WebServlet("/videoUpload")
public class VideoUploadController extends HttpServlet{ 
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String username = request.getHeader("username");
		String viewLevel = request.getHeader("viewLevel");
		String videoCategory = request.getHeader("videoCategory");
		String videoSubject = request.getHeader("videoSubject");
		String videoDescription = request.getHeader("videoDescription");
		String videoDate = request.getHeader("videoDate");
		File videoFile = null;
		if(ServletFileUpload.isMultipartContent(request)){
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                                         new DiskFileItemFactory()).parseRequest(request);
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                    	
                        String name = new File(item.getName()).getName();
                        item.write( videoFile = new File("/home/rilpa/" + File.separator + name));
                    }
                }
               //File uploaded successfully
                String videoId = UploadSQL.getInstance().uploadVideo(username, videoFile, videoSubject, videoDescription, 
                		videoCategory, viewLevel, videoDate);
                System.out.println("The video id is "+videoId.trim());
                response.setHeader("videoId", videoId.trim());
                
               
            } catch (Exception ex) {
               request.setAttribute("message", "File Upload Failed due to " + ex);
            }  
		}

	}
	
	

}
