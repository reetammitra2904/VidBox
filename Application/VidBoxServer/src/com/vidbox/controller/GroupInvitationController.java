package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vidbox.sql.GroupInvitationSQL;

@WebServlet("/sendInvitation")
public class GroupInvitationController extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int groupId = Integer.parseInt(request.getParameter("groupId"));
		String user = request.getParameter("user");
		String invitationMessage = request.getParameter("invitationMessage");
		System.out.println(groupId+" "+user+" "+invitationMessage);
		boolean result = GroupInvitationSQL.getInstance().insertGroupInvitation(groupId, user, invitationMessage);
		if (result) {
			response.setStatus(200);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
	}
	
	

}
