package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.vidbox.model.Group;
import com.vidbox.sql.CreateGroupSQL;

@WebServlet("/createGroup")
public class GroupCreationController extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String jsonString = request.getParameter("groupDetails");
		ObjectMapper mapper = new ObjectMapper();
		Group group = mapper.readValue(jsonString, Group.class);
		int groupId = CreateGroupSQL.getInstance().createGroup(group);
		if (groupId>0) {
			response.setStatus(200);
			response.setHeader("groupId", String.valueOf(groupId));
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

}
