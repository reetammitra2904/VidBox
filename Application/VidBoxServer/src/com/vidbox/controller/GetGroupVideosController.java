package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.vidbox.model.VideoList;
import com.vidbox.sql.FetchGroupVideosSQL;

@WebServlet("/fetchGroupVideos")
public class GetGroupVideosController extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int groupId = Integer.parseInt(request.getParameter("groupId"));
		VideoList videoList = new VideoList();
		videoList.setVideoList(FetchGroupVideosSQL.getInstance().fetchVideoList(groupId));
		ObjectMapper mapper = new ObjectMapper();
		String videoListJson = mapper.writeValueAsString(videoList);
		response.setHeader("videoList", videoListJson);
		response.setStatus(200);
		
	}
	
	

}
