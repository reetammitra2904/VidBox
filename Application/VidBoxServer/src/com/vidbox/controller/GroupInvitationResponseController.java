package com.vidbox.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vidbox.sql.GroupInvitationResponseSQL;

@WebServlet("/invitationResponse")
public class GroupInvitationResponseController extends HttpServlet{
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int groupInvitationId = Integer.parseInt(request.getParameter("groupInvitationId"));
		boolean userResponse = Boolean.parseBoolean(request.getParameter("response"));
		int groupId = Integer.parseInt(request.getParameter("groupId"));
		String username = request.getParameter("username");
		boolean result = GroupInvitationResponseSQL.getInstance().updateGroupInvitationResponse(groupInvitationId, userResponse, groupId, username);
		if (result) {
			response.setStatus(200);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
	}
	
	

}
