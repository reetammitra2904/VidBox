package com.vidbox.mongodb;

import java.io.File;
import java.io.IOException;

import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;
import com.vidbox.util.MongoDBConnection;

public class Upload {

	private GridFS gridfs;
	private GridFSInputFile inputFile;

	private static Upload upload = null;

	private Upload(){
		gridfs = new GridFS(MongoDBConnection.getDatabase());
	}

	public static Upload getInstance(){
		if (upload == null){
			upload = new Upload();
		}
		return upload;
	}

	public String addVideo (File video){				
		try {
			inputFile = gridfs.createFile(video);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("ID of file: " +inputFile.getId());

		//Save file to MongoDB
		inputFile.save();		
		//Return object id of the file
		return inputFile.getId().toString();
	}

}
