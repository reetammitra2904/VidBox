package com.vidbox.mongodb;

import java.io.File;
import java.io.IOException;

import org.bson.types.ObjectId;

import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.vidbox.util.MongoDBConnection;


public class Fetch {
	
	private static Fetch fetch = null;
	
	private Fetch() {
		
	}
	
	public static Fetch getInstance() {
		if (fetch == null) {
			fetch = new Fetch();
		}
		return fetch;
	}
		
	public File getVideo(String videoId){
	    File file = null;
		try {
			file = File.createTempFile("Test", "");
			GridFS gridfs = new GridFS(MongoDBConnection.getDatabase());
			// Find by ObjectId
			GridFSDBFile gridFSDBFile = gridfs.findOne(new ObjectId(videoId));	
			//System.out.println("Start time:" +System.currentTimeMillis());
			// Write gridfs file to disk
			gridFSDBFile.writeTo(file);
			//System.out.println("End time:" +System.currentTimeMillis());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;		
	}

}


