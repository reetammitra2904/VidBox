package com.vidbox.model;

import java.util.ArrayList;

public class VideoList {
	
	public ArrayList<Video> videoList;

	public ArrayList<Video> getVideoList() {
		return videoList;
	}

	public void setVideoList(ArrayList<Video> videoList) {
		this.videoList = videoList;
	}
	
	

}
