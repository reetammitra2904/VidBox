package com.vidbox.model;

import java.util.ArrayList;

public class LoginResponse {
	
	private ArrayList<Group> adminGroup;
	private ArrayList<Group> memberGroup;
	private VideoList videoList;
	private ArrayList<GroupInvitation> invitationList;
	public ArrayList<Group> getAdminGroup() {
		return adminGroup;
	}
	public void setAdminGroup(ArrayList<Group> adminGroup) {
		this.adminGroup = adminGroup;
	}
	public ArrayList<Group> getMemberGroup() {
		return memberGroup;
	}
	public void setMemberGroup(ArrayList<Group> memberGroup) {
		this.memberGroup = memberGroup;
	}
	
	public VideoList getVideoList() {
		return videoList;
	}
	public void setVideoList(VideoList videoList) {
		this.videoList = videoList;
	}
	public ArrayList<GroupInvitation> getInvitationList() {
		return invitationList;
	}
	public void setInvitationList(ArrayList<GroupInvitation> invitationList) {
		this.invitationList = invitationList;
	}
	
	

}
