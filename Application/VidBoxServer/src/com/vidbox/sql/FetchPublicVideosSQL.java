package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.vidbox.model.Video;
import com.vidbox.util.DatabaseConnection;

public class FetchPublicVideosSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
    private static FetchPublicVideosSQL fetchPublicVideosSQL = null;
	
	private FetchPublicVideosSQL() {
		
	}
	
	public static FetchPublicVideosSQL getInstance() {
		if (fetchPublicVideosSQL == null) {
			fetchPublicVideosSQL = new FetchPublicVideosSQL();
		}
		return fetchPublicVideosSQL;
	}
	
	public ArrayList<Video> fetchVideoList() {
		
		ArrayList<Video> videoList = new ArrayList<Video>();
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from video where viewLevel = 'Public'");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Video video = new Video();
				video.setVideoId(resultSet.getString("videoId"));
				video.setVideoTitle(resultSet.getString("title"));
				video.setVideoDescription(resultSet.getString("description"));
				video.setCategory(resultSet.getString("category"));
				video.setUsername(resultSet.getString("username"));
				video.setDate(resultSet.getString("date"));
				video.setViewLevel(resultSet.getString("viewLevel"));
				videoList.add(video);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return videoList;
		
		
	}

	

}
