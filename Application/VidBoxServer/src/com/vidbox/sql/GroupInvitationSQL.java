package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vidbox.util.DatabaseConnection;

public class GroupInvitationSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static GroupInvitationSQL groupInvitationSQL = null;
	
	private GroupInvitationSQL() {
		
	}
	
	public static GroupInvitationSQL getInstance() {
		if (groupInvitationSQL == null) {
			groupInvitationSQL = new GroupInvitationSQL();
		}
		return groupInvitationSQL;
	}
	
	public boolean insertGroupInvitation (int groupId, String user, String invitationMessage) {
		try {
			PreparedStatement usernamePreparedStatement = connection.prepareStatement("select * from user where username = ?");
			usernamePreparedStatement.setString(1, user);
			ResultSet resultSet = usernamePreparedStatement.executeQuery();
			if (resultSet.next()) {
				PreparedStatement preparedStatement = connection.prepareStatement("insert into groupInvitation (groupId, userId, invitationMessage) values (?,?,?)");
				preparedStatement.setInt(1, groupId);
				preparedStatement.setString(2, user);
				preparedStatement.setString(3, invitationMessage);
				int result = preparedStatement.executeUpdate();
				if (result > 0) {
					return true;
				}
			} else {
				return false;
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
