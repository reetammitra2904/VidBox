package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.vidbox.model.Group;
import com.vidbox.util.DatabaseConnection;

public class FetchMemberGroupSQL {
	
private static FetchMemberGroupSQL fetchMemberGroupSQL = null;
	
	private FetchMemberGroupSQL() {
		
	}
	
	public static FetchMemberGroupSQL getInstance() {
		if (fetchMemberGroupSQL == null) {
			fetchMemberGroupSQL = new FetchMemberGroupSQL();
		}
		return fetchMemberGroupSQL;
	}
	
	private Connection connection = DatabaseConnection.getConnection();
	
	public ArrayList<Group> getMemberGroup(String username) {
	    ArrayList<Group> memberGroupList = new ArrayList<Group>();
	    try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from group_t where groupId IN (select groupId from join_t where userId = ?)");
			preparedStatement.setString(1, username);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Group group = new Group();
				group.setAdmin(resultSet.getString("adminId"));
				group.setGroupCategory(resultSet.getString("groupCategory"));
				group.setGroupDescription(resultSet.getString("groupDescription"));
				group.setGroupName(resultSet.getString("groupName"));
				group.setGroupShareLevel(resultSet.getString("groupShareLevel"));
				group.setGroupType(resultSet.getString("groupType"));
				group.setGroupId(resultSet.getInt("groupId"));
				memberGroupList.add(group);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return memberGroupList;
	}

}
