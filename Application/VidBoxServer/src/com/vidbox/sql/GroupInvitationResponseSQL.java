package com.vidbox.sql;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import com.vidbox.util.DatabaseConnection;

public class GroupInvitationResponseSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static GroupInvitationResponseSQL groupInvitationResponseSQL = null;
	
	private GroupInvitationResponseSQL() {
		
	}
	
	public static GroupInvitationResponseSQL getInstance() {
		if (groupInvitationResponseSQL == null) {
			groupInvitationResponseSQL = new GroupInvitationResponseSQL();
		}
		return groupInvitationResponseSQL;
	}
	
	public boolean updateGroupInvitationResponse(int groupInvitationId, boolean userResponse, int groupId, String username) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("delete from groupInvitation where "
					+ "groupInvitationId = ?");
			preparedStatement.setInt(1, groupInvitationId);
			int result = preparedStatement.executeUpdate();
			if (result > 0 && userResponse) {
				PreparedStatement memberPreparedStatement = connection.prepareStatement("insert into join_t (groupId, userId, dateInfo)"
						+ " values (?,?,?)");
				memberPreparedStatement.setInt(1, groupId);
				memberPreparedStatement.setString(2, username);
				memberPreparedStatement.setString(3, Calendar.getInstance().getTime().toString());
				int memberResult = memberPreparedStatement.executeUpdate();
				if (memberResult > 0) {
					return true;
				} else {
					return false;
				}			
			} else {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
