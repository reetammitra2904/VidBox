package com.vidbox.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.vidbox.mongodb.Upload;

import com.vidbox.util.DatabaseConnection;

public class UploadSQL {
	private Connection connection = DatabaseConnection.getConnection();

	private static UploadSQL uploadSQL = null;

	private UploadSQL() {
	}

	public static UploadSQL getInstance() {
		if (uploadSQL == null) {
			uploadSQL = new UploadSQL();
		}
		return uploadSQL;
	}

	public String uploadVideo (String userId, File video, String videoTitle, String videoDescription, String videoCategory, String viewLevel, String date){
		Upload upload = Upload.getInstance();
		String videoId = upload.addVideo(video);		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("insert into video (videoId, username, title, description, category, viewLevel, date) values (?,?,?,?,?,?,?)");
			preparedStatement.setString(1, videoId);
			preparedStatement.setString(2, userId);
			preparedStatement.setString(3, videoTitle);
			preparedStatement.setString(4, videoDescription);
			preparedStatement.setString(5, videoCategory);
			preparedStatement.setString(6, viewLevel);
			preparedStatement.setString(7, date);

			int result = preparedStatement.executeUpdate();
			if (result > 0) {
				return videoId;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
