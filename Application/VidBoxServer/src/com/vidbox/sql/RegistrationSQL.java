package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vidbox.util.DatabaseConnection;

public class RegistrationSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static RegistrationSQL registrationSQL = null;
	
	private RegistrationSQL() {
		
	}
	
	public static RegistrationSQL getInstance() {
		if (registrationSQL == null) {
			registrationSQL = new RegistrationSQL();
		}
		return registrationSQL;
	}
	
	public boolean registerUser(String name, String username, String password, String emailAddress) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("insert into user (name, username,password,"
					+ "emailAddress) values (?,?,?,?)");
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, username);
			preparedStatement.setString(3, password);
			preparedStatement.setString(4, emailAddress);
			int result = preparedStatement.executeUpdate();
			if (result > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean checkUsername (String username) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from user where username = ?");
			preparedStatement.setString(1, username);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
		
	}

}
