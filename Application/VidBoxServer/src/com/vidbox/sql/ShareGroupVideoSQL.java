package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.vidbox.util.DatabaseConnection;

public class ShareGroupVideoSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static ShareGroupVideoSQL shareGroupVideoSQL = null;
	
	private ShareGroupVideoSQL() {
		
	}
	
	public static ShareGroupVideoSQL getInstance() {
		if (shareGroupVideoSQL == null) {
			shareGroupVideoSQL = new ShareGroupVideoSQL();
		}
		return shareGroupVideoSQL;
	}
	
	public boolean shareGroupVideo(String user, int groupId, String videoId) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("insert into groupShare (groupId, videoId, userId)"
					+ " values(?,?,?)");
			preparedStatement.setInt(1, groupId);
			preparedStatement.setString(2, videoId);
			preparedStatement.setString(3, user);
			int result = preparedStatement.executeUpdate();
			if (result > 0) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
