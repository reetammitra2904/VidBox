package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vidbox.model.Group;
import com.vidbox.util.DatabaseConnection;

public class CreateGroupSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static CreateGroupSQL createGroupSQL = null;
	
	private CreateGroupSQL() {
		
	}
	
	public static CreateGroupSQL getInstance() {
		if (createGroupSQL == null) {
			createGroupSQL = new CreateGroupSQL();
		}
		return createGroupSQL;
	}
	
	public int createGroup(Group group) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("insert into group_t (groupName, groupCategory"
					+ ", groupDescription, groupShareLevel, groupType, adminId) values (?,?,?,?,?,?)");
			preparedStatement.setString(1, group.getGroupName());
			preparedStatement.setString(2, group.getGroupCategory());
			preparedStatement.setString(3, group.getGroupDescription());
			preparedStatement.setString(4, group.getGroupShareLevel());
			preparedStatement.setString(5, group.getGroupType());
			preparedStatement.setString(6, group.getAdmin());
			int result = preparedStatement.executeUpdate();
			if (result > 0) {
				PreparedStatement groupIdPreparedStatement = connection.prepareStatement("select max(groupId) from group_t where adminId = ?");
				groupIdPreparedStatement.setString(1, group.getAdmin());
				ResultSet groupIdResultSet = groupIdPreparedStatement.executeQuery();
				while(groupIdResultSet.next()) {
					return groupIdResultSet.getInt("max(groupId)");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
