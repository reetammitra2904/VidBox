package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vidbox.util.DatabaseConnection;

public class LoginSQL {

	private Connection connection = DatabaseConnection.getConnection();
	
	private static LoginSQL loginSQL = null;
	
	private LoginSQL() {
		
	}
	
	public static LoginSQL getInstance() {
		if (loginSQL == null) {
			loginSQL = new LoginSQL();
		}
		return loginSQL;
	}
	
	public boolean loginUser(String username, String password) {
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from user  where username = ? and password = ?");
			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
	
}
