package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.vidbox.model.Video;
import com.vidbox.util.DatabaseConnection;

public class FetchSharedVideosSQL {
	
    private Connection connection = DatabaseConnection.getConnection();
	
	private static FetchSharedVideosSQL fetchSharedVideosSQL = null;
	
	private FetchSharedVideosSQL() {
		
	}
	
	public static FetchSharedVideosSQL getInstance() {
		if (fetchSharedVideosSQL == null) {
			fetchSharedVideosSQL = new FetchSharedVideosSQL();
		}
		return fetchSharedVideosSQL;
	}
	
	public ArrayList<Video> fetchVideoList(String username) {
		
		ArrayList<Video> videoList = new ArrayList<Video>();
		
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from video where videoId in (select videoId from peerShare where pUserId = ?)");
			preparedStatement.setString(1, username);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Video video = new Video();
				video.setVideoId(resultSet.getString("videoId"));
				video.setVideoTitle(resultSet.getString("title"));
				video.setVideoDescription(resultSet.getString("description"));
				video.setCategory(resultSet.getString("category"));
				video.setUsername(username);
				video.setDate(resultSet.getString("date"));
				video.setViewLevel(resultSet.getString("viewLevel"));
				videoList.add(video);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return videoList;
		
		
	}

}
