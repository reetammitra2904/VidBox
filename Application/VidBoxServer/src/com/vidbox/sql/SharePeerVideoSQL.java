package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vidbox.util.DatabaseConnection;

public class SharePeerVideoSQL {
	
	private Connection connection = DatabaseConnection.getConnection();
	
	private static SharePeerVideoSQL sharePeerVideoSQL = null;
	
	private SharePeerVideoSQL() {
		
	}
	
	public static SharePeerVideoSQL getInstance() {
		if (sharePeerVideoSQL == null) {
			sharePeerVideoSQL = new SharePeerVideoSQL();
		}
		return sharePeerVideoSQL;
	}
	
	public boolean sharePeerVideo(String user, String peerUser, String videoId) {
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select * from user where username = ?");
			preparedStatement.setString(1, peerUser);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				PreparedStatement peerSharePreparedStatement = connection.prepareStatement("insert into peerShare (userId, pUserId, videoId) values (?,?,?)");
				peerSharePreparedStatement.setString(1, user);
				peerSharePreparedStatement.setString(2, peerUser);
				peerSharePreparedStatement.setString(3, videoId);
				int result = peerSharePreparedStatement.executeUpdate();
				if (result > 0) {
					return true;
				}
			} else {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
