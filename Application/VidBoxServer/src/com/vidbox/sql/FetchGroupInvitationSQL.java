package com.vidbox.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.vidbox.model.Group;
import com.vidbox.model.GroupInvitation;
import com.vidbox.util.DatabaseConnection;

public class FetchGroupInvitationSQL {
	
	private static FetchGroupInvitationSQL fetchGroupInvitationSQL = null;
	
	private FetchGroupInvitationSQL() {
		
	}
	
	public static FetchGroupInvitationSQL getInstance() {
		if (fetchGroupInvitationSQL == null) {
			fetchGroupInvitationSQL = new FetchGroupInvitationSQL();
		}
		return fetchGroupInvitationSQL;
	}
	
	private Connection connection = DatabaseConnection.getConnection();
	
	public ArrayList<GroupInvitation> fetchGroupInvitations(String username) {
		ArrayList<GroupInvitation> groupInvitationList = new ArrayList<GroupInvitation>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("select "
					+ "groupInvitation.invitationMessage, groupInvitation.groupInvitationId, group_t.groupId, "
					+ "group_t.adminId, group_t.groupName, group_t.groupType, group_t.groupCategory, "
					+ "group_t.groupShareLevel, group_t.groupDescription from groupInvitation, group_t "
					+ "where groupInvitation.userId = ? and group_t.groupId = groupInvitation.groupId");
			preparedStatement.setString(1, username);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				GroupInvitation groupInvitation = new GroupInvitation();
				Group group = new Group();
				group.setAdmin(resultSet.getString("adminId"));
				group.setGroupCategory(resultSet.getString("groupCategory"));
				group.setGroupDescription(resultSet.getString("groupDescription"));
				group.setGroupName(resultSet.getString("groupName"));
				group.setGroupShareLevel(resultSet.getString("groupShareLevel"));
				group.setGroupType(resultSet.getString("groupType"));
				group.setGroupId(resultSet.getInt("groupId"));
				groupInvitation.setGroup(group);
				groupInvitation.setInvitationMessage(resultSet.getString("invitationMessage"));
				groupInvitation.setUser(username);
				groupInvitation.setGroupInvitationId(resultSet.getInt("groupInvitationId"));
				groupInvitationList.add(groupInvitation);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return groupInvitationList;
	}

}
