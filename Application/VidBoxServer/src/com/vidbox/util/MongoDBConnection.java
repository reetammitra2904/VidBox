package com.vidbox.util;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

public class MongoDBConnection {
	
	private static MongoClient client = null;
	
	public static MongoClient getClient(){
		if(client == null){
			//client = new MongoClient("192.168.0.19",27017);
			client = new MongoClient("10.42.0.156",27017);
		}
		return client;
	}
	
	public static DB getDatabase(){
		return MongoDBConnection.getClient().getDB("vidbox");		
	}
	
	public static DBCollection getCollection(){
		return MongoDBConnection.getClient().getDB("vidbox").getCollection("videoDB");
	}

}
